import { Pipe, PipeTransform } from '@angular/core';
import { config } from './constant';

@Pipe({
  name: 'linkAssets'
})
export class LinkAssetsPipe implements PipeTransform {

  transform(value: string): string {
    return config.API_URL + 'uploads/image/' + value;
  }

}
