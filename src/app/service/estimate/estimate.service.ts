import { Injectable } from '@angular/core';
import { config } from '../../constant'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EstimateService {
  public parentType = 'type';
  public integrationPrice = 200;
  public integrationConceptionPrice = 400;
  public idea = 500;
  public draft = 750;
  public progress = 1000;
  public maintenance = 1250;

  public estimateLists = []
  public total = 0;
  public totalInit = 0;
  public isDesign = false;
  public isLevel = false;
  public designValue = 0;
  public levelValue = 0;

  flushPrice() {
    let total = 0;
    total += this.totalInit;
    for (const estimation of this.estimateLists) {
      if (estimation.isChecked) {
        total += estimation.price;
      }
    }
    total += this.designValue;
    total += this.levelValue;
    this.total = total;
  }

  async findEstimationData() {
    this.http.get(config.API_URL + 'estimation/api/' + this.parentType + '/get/estimation').subscribe(data => {
      this.estimateLists = data[0].estimations;
      this.totalInit = data[0].price;
      this.total = this.totalInit;
    })
  }

  constructor(public http: HttpClient) { }

}
