import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from './../../constant';
import { CommonFrontService } from '../common/common-front-service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient,private commonService:CommonFrontService) { }

  async getContent() {
    this.http.get(config.API_URL + 'api/get/content/').subscribe((data) => {
      this.commonService.dataFromDb = data;
    });
  }
}
