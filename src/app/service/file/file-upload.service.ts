import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private httpClient: HttpClient) { }

  public upload(data){
    const formData: FormData = new FormData();
    for (const [key,value] of data.data) {
      console.log(key,value);
    }
  }
}
