import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonFrontService {
  public routeState = 'home';
  public dataFromDb: any;

  public isWebCreate = true;
  public isWebGraph = false;
  constructor() { }
}
