import { TestBed } from '@angular/core/testing';

import { CommonFrontService } from './common-front-service';

describe('CommonFrontServiceService', () => {
  let service: CommonFrontService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommonFrontService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
