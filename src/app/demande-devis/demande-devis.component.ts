import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FileUploadService } from './../service/file/file-upload.service'

@Component({
  selector: 'app-demande-devis',
  templateUrl: './demande-devis.component.html',
  styleUrls: ['./demande-devis.component.scss']
})
export class DemandeDevisComponent implements OnInit {
  quotationAskData = new FormGroup({
    societyName: new FormControl('', Validators.required),
    interlocutorName: new FormControl('', Validators.required),
    mail: new FormControl('', Validators.required),
    tel: new FormControl('', Validators.required),
    natureOfNeed: new FormControl('', Validators.required),
    files: new FormControl('', Validators.required),
  })
  files = [];
  constructor(public uploadService: FileUploadService) { }

  submitForm() {
    let finalData = {
      data: this.quotationAskData.value,
      files: this.files
    }
    this.uploadService.upload(finalData);
  }

  ngOnInit(): void {
  }

  onFileChange(event) {
    this.files.push(event.target.files[0]);
  }
}
