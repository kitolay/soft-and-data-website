import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from 'src/app/service/common/common-front-service';
import { MatDialog } from '@angular/material/dialog';
import { DialogPortfolioComponent } from './dialog-portfolio/dialog-portfolio.component';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  constructor(public commonSevice: CommonFrontService, public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openDialog(data) {
    const dialogRef = this.dialog.open(DialogPortfolioComponent, {
      width: '1000px',
      height: '500px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }
}
