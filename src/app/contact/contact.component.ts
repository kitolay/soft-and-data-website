import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from '../service/common/common-front-service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { config } from './../constant'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactValue = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    subject: new FormControl('', Validators.required),
    message: new FormControl('', Validators.required),
  })
  constructor(public commonFrontService: CommonFrontService, public http: HttpClient) { }

  ngOnInit(): void {
    this.commonFrontService.routeState = 'home';
  }

  onSubmit() {
    this.http.post(config.API_URL + 'api/services/sendMailInContact', this.contactValue.value).subscribe((result) => {
      console.log(result);
    })
  }
}
