import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Page } from './../../../assets/slider/js/slider_custom';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    Page.init();
  }

}
