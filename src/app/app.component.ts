import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from './service/common/common-front-service';
import { Router, NavigationEnd } from '@angular/router';
import { ApiService } from './service/api/api.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'tech';
  activeLink = '/home';
  constructor(public commonFrontService: CommonFrontService, private router: Router, public api: ApiService) { }

  ngOnInit() {
    this.commonFrontService.routeState = 'home';
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      this.activeLink = this.router.url;
      // this.api.getContent();
      window.scrollTo(0, 0);
    })
    this.api.getContent();
  }
}
