import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from 'src/app/service/common/common-front-service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DemandeDevisComponent } from '../demande-devis/demande-devis.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(public commonService: CommonFrontService, public dialog: MatDialog) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(DemandeDevisComponent, {
      width: '1000px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  doAutoScrol(){
    window.scroll(0,0);
  }
  
  ngOnInit(): void {
  }

}
