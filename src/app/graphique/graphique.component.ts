import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from 'src/app/service/common/common-front-service';
@Component({
  selector: 'app-graphique',
  templateUrl: './graphique.component.html',
  styleUrls: ['./graphique.component.scss']
})
export class GraphiqueComponent implements OnInit {

  constructor(public commonService: CommonFrontService) { }

  ngOnInit(): void {
    this.commonService.routeState = 'home';
  }

}
