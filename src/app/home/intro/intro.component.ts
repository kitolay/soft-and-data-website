import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {Router} from '@angular/router';
import { DemandeDevisComponent } from '../../demande-devis/demande-devis.component';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
  @Input() logoLink:any ="";
  constructor(public dialog: MatDialog,private routes: Router,) { }

  ngOnInit(): void {
  }
  toEstimation(){
    this.routes.navigate(['/estimate']);
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DemandeDevisComponent, {
      width: '1000px',
    });
  }
}
