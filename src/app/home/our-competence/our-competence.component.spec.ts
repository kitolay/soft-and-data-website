import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurCompetenceComponent } from './our-competence.component';

describe('OurCompetenceComponent', () => {
  let component: OurCompetenceComponent;
  let fixture: ComponentFixture<OurCompetenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurCompetenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurCompetenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
