import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from '../service/common/common-front-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public commonFrontService: CommonFrontService) { }

  ngOnInit(): void {
    this.commonFrontService.routeState = 'home';
  }

}
