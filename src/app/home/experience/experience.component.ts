import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from 'src/app/service/common/common-front-service';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  dataFromDb = {
    experience: {
      titre1: 'Nos expériences',
      titre2: '',
      img: '',
      description: '',
      full_description: '',
      child: [
        {
          titre1: 'php & mysql',
          titre2: '',
          img: '',
          description: '33%',
          full_description: '',
        },
        {
          titre1: 'symfony',
          titre2: '',
          img: '',
          description: '50%',
          full_description: '',
        },
        {
          titre1: 'angular',
          titre2: '',
          img: '',
          description: '92%',
          full_description: '',
        },
        {
          titre1: 'Laravel',
          titre2: '',
          img: '',
          description: '40%',
          full_description: '',
        },
        {
          titre1: 'php & mysql',
          titre2: '',
          img: '',
          description: '33%',
          full_description: '',
        },
        {
          titre1: 'symfony',
          titre2: '',
          img: '',
          description: '50%',
          full_description: '',
        },
        {
          titre1: 'angular',
          titre2: '',
          img: '',
          description: '92%',
          full_description: '',
        },
        {
          titre1: 'Laravel',
          titre2: '',
          img: '',
          description: '40%',
          full_description: '',
        }

      ]
    }
  }
  constructor(public commonService: CommonFrontService) { }

  ngOnInit(): void {
    console.log(this.commonService.dataFromDb);
  }

}
