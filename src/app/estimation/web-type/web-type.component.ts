import { Component, OnInit } from '@angular/core';
import { EstimateService } from 'src/app/service/estimate/estimate.service';

@Component({
  selector: 'app-web-type',
  templateUrl: './web-type.component.html',
  styleUrls: ['./web-type.component.scss', '../estimation.component.scss']
})
export class WebTypeComponent implements OnInit {

  constructor(public estimateService: EstimateService) { }

  ngOnInit(): void {
  }

}
