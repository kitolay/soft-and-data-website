import { Component, OnInit } from '@angular/core';
import { EstimateService } from 'src/app/service/estimate/estimate.service';

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.scss']
})
export class DesignComponent implements OnInit {

  constructor(public estimateService:EstimateService) { }

  ngOnInit(): void {
  }

}
