import { Component, OnInit } from '@angular/core';
import { EstimateService } from 'src/app/service/estimate/estimate.service';

@Component({
  selector: 'app-type-web-mobile',
  templateUrl: './type-web-mobile.component.html',
  styleUrls: ['./type-web-mobile.component.scss','./../estimation.component.scss']
})
export class TypeWebMobileComponent implements OnInit {

  constructor(public estimateService: EstimateService) { }

  ngOnInit(): void {
  }

}
