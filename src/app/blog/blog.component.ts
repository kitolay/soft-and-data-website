import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from '../service/common/common-front-service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  constructor(public commonFrontService:CommonFrontService) { }

  ngOnInit(): void {
    this.commonFrontService.routeState = 'home';
  }

}
