import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-processus',
  templateUrl: './processus.component.html',
  styleUrls: ['./processus.component.scss']
})
export class ProcessusComponent implements OnInit {
  contentdata:any = resdata;

  constructor() { }

  ngOnInit(): void {
  }

}

var resdata = {
  title:"Processus",
  intro:[
    {
      title:"Cycle de vie de votre projet",
      description:`Nous accompagnons nos clients tout le long du cycle de vie de leurs projets, grâce à la performance et rigueur de notre équipe d’Ingénieurs expérimentés, qui leur assurent des travaux de haut niveau.`
    },
    {
      title:"Notre processus de travail",
      description:`Nous avons mis au point une méthode de travail unique qui nous permet d'obtenir, pour nos clients, d'excellents résultats, peu importe la complexité de leur projet.`
    }
  ],
  elements:[
    {
      title:"Processus pour la création web et applications",
      dataliste:[
        {
          title:"Conseil",
          num:"1",
          img:"assets/processus/undraw_informed_decision_p2lh.svg",
          description:`Nous vous proposons la solution idéale pour répondre exactement à
          vos attentes, en optimisant le coût et en s'engageant sur un meilleur délai. Nous vous accompagnons ainsi
          dans le cadre de l'analyse fonctionnelle de votre projet à partir d'une expression de vos besoins, vous
          accompagner dans la rédaction d'un cahier des charges s'il n'existe pas encore, ou le cas contraire, dans
          l'étude approfondie de celui-ci.`
        },
        {
          title:"Design",
          num:"2",
          img:"assets/processus/undraw_wireframing_nxyi.svg",
          description:`Notre équipe de UX/UI Designers travaille étroitement avec nos
          Ingénieurs, dans la réalisation de maquette et de charte graphique. Pour s’assurer de l’efficacité et de la
          pertinence de vos applications, dans la mesure où elles seront simples à utiliser, mais également adaptées à
          un large éventail d'usages, toujours basé sur les besoins de l'utilisateur.`
        },
        {
          title:"Analyse",
          num:"3",
          img:"assets/processus/undraw_detailed_analysis_xn7y.svg",
          description:`Nous vous accompagnons dans la concrétisation de vos idées, sur
          base d'un cahier des charges initial, détaillant l'ensemble des aspects techniques et fonctionnels de votre
          projet, pour une mise en oeuvre efficace. Ainsi, nous nous engageons à vous livrer un site ou une
          application robuste, maintenable et personnalisé(e), avec une approche centrée sur l'utilisateur.`
        },
        {
          title:"Developement",
          num:"4",
          img:"assets/processus/undraw_programming_2svr.svg",
          description:`Notre expertise nous permet de répondre à vos demandes les plus
          complexes, avec un accompagnement de bout en bout pendant la réalisation de votre projet. Une équipe
          d'Ingénieurs expérimentés se met à votre disposition pour assurer une programmation de haut niveau,
          conformément aux besoins spécifiés dans le cahier des charges.`
        },
        {
          title:"Test & qualite",
          num:"5",
          img:"assets/processus/undraw_mobile_testing_reah.svg",
          description:`Une équipe agile et dédiée, d'Ingénieurs, experts, se mobilise pour
          assurer une qualité irréprochable de vos livrables. Avec une maîtrise parfaite de la méthodologie SCRUM, sur
          base de l'itératif et l'incrémental.`
        },
        {
          title:"Deploiement",
          num:"6",
          img:"assets/processus/undraw_launching_125y.svg",
          description:`Dernière étape tactique, mais très importante ! Après son passage
          sous les mains expertes de nos ingénieurs qualité, nous vous garantissons un solide processus de mise en
          production de votre projet fini et son bon fonctionnement. Notre engagement ? Un grand respect des normes de
          déploiement, avec les outils les plus adaptés.`
        },
        {
          title:"Maintenance",
          num:"7",
          img:"assets/processus/undraw_maintenance_cn7j.svg",
          description:`Pour l'ensemble des projets que nous traitons, nous garantissons un
          service de maintenance corrective (selon le délai préétabli en démarrage des travaux), ou évolutive en
          fonction de vos besoins. Possibilité également de mettre à votre disposition, des ressources dédiées pour un
          besoin ponctuel.`
        }
      ]
    },
    {
      title:"Processus Pour la creation graphique",
      dataliste:[
        {
          title:"Conseil",
          num:"1",
          img:"assets/processus/undraw_creativity_wqmm.svg",
          description:`Nous vous proposons la solution idéale pour répondre exactement à
          vos attentes, en optimisant le coût et en s'engageant sur un meilleur délai. Nous vous accompagnons ainsi
          dans le cadre de l'analyse fonctionnelle de votre projet à partir d'une expression de vos besoins, vous
          accompagner dans la rédaction d'un cahier des charges s'il n'existe pas encore, ou le cas contraire, dans
          l'étude approfondie de celui-ci.`
        },
        {
          title:"Design",
          num:"2",
          img:"assets/processus/undraw_image_post_24iy.svg",
          description:`Notre équipe de UX/UI Designers travaille étroitement avec nos
          Ingénieurs, dans la réalisation de maquette et de charte graphique. Pour s’assurer de l’efficacité et de la
          pertinence de vos applications, dans la mesure où elles seront simples à utiliser, mais également adaptées à
          un large éventail d'usages, toujours basé sur les besoins de l'utilisateur.`
        },
        {
          title:"Analyse",
          num:"3",
          img:"assets/processus/undraw_analyze_17kw.svg",
          description:`Nous vous accompagnons dans la concrétisation de vos idées, sur
          base d'un cahier des charges initial, détaillant l'ensemble des aspects techniques et fonctionnels de votre
          projet, pour une mise en oeuvre efficace. Ainsi, nous nous engageons à vous livrer un site ou une
          application robuste, maintenable et personnalisé(e), avec une approche centrée sur l'utilisateur.`
        },
        {
          title:"Developement",
          num:"4",
          img:"assets/processus/undraw_Work_time_re_hdyv.svg",
          description:`Notre expertise nous permet de répondre à vos demandes les plus
          complexes, avec un accompagnement de bout en bout pendant la réalisation de votre projet. Une équipe
          d'Ingénieurs expérimentés se met à votre disposition pour assurer une programmation de haut niveau,
          conformément aux besoins spécifiés dans le cahier des charges.`
        },
        {
          title:"Test & qualite",
          num:"5",
          img:"assets/processus/undraw_code_inspection_bdl7.svg",
          description:`Une équipe agile et dédiée, d'Ingénieurs, experts, se mobilise pour
          assurer une qualité irréprochable de vos livrables. Avec une maîtrise parfaite de la méthodologie SCRUM, sur
          base de l'itératif et l'incrémental.`
        },
        {
          title:"Deploiement",
          num:"6",
          img:"assets/processus/undraw_Steps_re_odoy.svg",
          description:`Dernière étape tactique, mais très importante ! Après son passage
          sous les mains expertes de nos ingénieurs qualité, nous vous garantissons un solide processus de mise en
          production de votre projet fini et son bon fonctionnement. Notre engagement ? Un grand respect des normes de
          déploiement, avec les outils les plus adaptés.`
        },
        {
          title:"Maintenance",
          num:"7",
          img:"assets/processus/undraw_clean_up_ucm0.svg",
          description:`Pour l'ensemble des projets que nous traitons, nous garantissons un
          service de maintenance corrective (selon le délai préétabli en démarrage des travaux), ou évolutive en
          fonction de vos besoins. Possibilité également de mettre à votre disposition, des ressources dédiées pour un
          besoin ponctuel.`
        }
      ]
    }
  ]
}
