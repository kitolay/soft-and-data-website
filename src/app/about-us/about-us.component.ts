import { Component, Inject, NgModule, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from './../constant';
import { CommonFrontService } from '../service/common/common-front-service';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  allTeam: any = [];
  cardswitch: any;
  imgLinkList:any = [];
  colorList:any = ["#ef476f","#ffd166","#06d6a0","#118ab2","#073b4c","#e07a5f","#fb8500","#ffb703","#023047","#9e2a2b","#8338ec"];

  constructor(private http: HttpClient, public commonService: CommonFrontService, private _bottomSheet: MatBottomSheet ,config: NgbCarouselConfig) {
    config.interval = 5000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
   }
  // constructor() { }

  ngOnInit(): void {
    this.http.get(config.API_URL + 'team/find/all/type').subscribe((allTeam) => {
      this.allTeam = allTeam;
      let spacebuff = false;
      this.cardswitch = this.allTeam.map(()=>{spacebuff = !spacebuff; return {top:spacebuff,botomn:!spacebuff}});
    });
    console.log("kakana sy voanjobory", this.commonService.dataFromDb.about_slide);
  }
  getCardColor(){
    return "background-color: "+this.colorList[Math.floor(Math.random() * this.colorList.length)]+"; min-height: 200px;";
  }
  getImgList(){
    this.imgLinkList = this.commonService.dataFromDb.about_slide[0].childs.map(itm=>{return itm.img});
    if(this.imgLinkList.length > 0) return true; else return false;
  }
  openBottomSheet(team) {
    this._bottomSheet.open(CvSheet, {
      data: team
    });
  }
}

@Component({
  selector: 'cv-sheet',
  templateUrl: 'cv.html',
  styleUrls: ['cv.scss']
})
export class CvSheet {
  constructor(private _bottomSheetRef: MatBottomSheetRef<CvSheet>, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
    console.log(this.data);
  }

  linkAsset(value: string): string {
    return config.API_URL + 'uploads/image/' + value;
  }

  openLink(): void {
    this._bottomSheetRef.dismiss();
  }
}


