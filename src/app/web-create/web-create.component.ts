import { Component, OnInit, HostListener } from '@angular/core';
import { CommonFrontService } from 'src/app/service/common/common-front-service';
@Component({
  selector: 'app-web-create',
  templateUrl: './web-create.component.html',
  styleUrls: ['./web-create.component.scss']
})
export class WebCreateComponent implements OnInit {
  datacontent:any = [];
  menucontainer:any;

  curentpage:any = 1;
  bannerlink:any = "headimg d-flex flex-row justify-content-between";
  backlink:any = "maincont d-flex flex-column";
  color2:any = "mybtstyle btstylelg btstylemd";
  tabTextColor:any = "bttext";
  firstload:any = true;
  posfromload:any = true;

  inactivTabClass:any = "btstylelg btstylemd stinactive";
  
  tabdata1:any = [];
  tabdata2:any = [];

  showingtab:any;

  constructor(public commonService: CommonFrontService) {
  }

  @HostListener('window:scroll', ['$event']) 
  doSomething(event) {
    if(window.pageYOffset > 1000) {
      this.commonService.isWebGraph = true;
    } else {
      this.commonService.isWebGraph = false;
    }
  }

  ngOnInit(): void {
    this.commonService.routeState = 'home';
    console.log("THERSE DATA",history.state);
  }
  getDatas(){
    if(this.firstload){
      this.initdatas();
      this.changePage();
      return 1;
    }
    else return 2;
  }
  initdatas(){
      this.tabdata1 = this.commonService.dataFromDb.web_create[0].childs.map((itm,i)=>{return {c2:this.color2+" stinactive",bclass:"container mcustumbt mycusbtn d-flex d-sm-flex d-md-flex flex-column flex-sm-column flex-md-row hvr-bubble-float-right2 inactive",active:false,index:i,data:itm}});
      this.tabdata2 = this.commonService.dataFromDb.graphic[0].childs.map((itm,i)=>{return {c2:this.color2+" stinactive",bclass:"container mcustumbt mycusbtn d-flex d-sm-flex d-md-flex flex-column flex-sm-column flex-md-row hvr-bubble-float-right2 inactive",active:false,index:i,data:itm}});
      if(this.posfromload && history.state.data != undefined && history.state.data != null && history.state.data.id != ""){
        let id = history.state.data.id;
        this.tabdata1[id].c2 = this.color2;
        this.tabdata1[id].bclass = "container mcustumbt mycusbtn d-flex d-sm-flex d-md-flex flex-column flex-sm-column flex-md-row hvr-bubble-float-right";
        this.tabdata2[0].c2 = this.color2;
        this.tabdata2[0].bclass = "container mcustumbt mycusbtn d-flex d-sm-flex d-md-flex flex-column flex-sm-column flex-md-row hvr-bubble-float-right";
        
        this.menucontainer = this.tabdata1[id].data;
        this.posfromload = false;
      }
      else{
        this.tabdata1[0].c2 = this.color2;
        this.tabdata1[0].bclass = "container mcustumbt mycusbtn d-flex d-sm-flex d-md-flex flex-column flex-sm-column flex-md-row hvr-bubble-float-right";
        this.tabdata2[0].c2 = this.color2;
        this.tabdata2[0].bclass = "container mcustumbt mycusbtn d-flex d-sm-flex d-md-flex flex-column flex-sm-column flex-md-row hvr-bubble-float-right";
        
        this.menucontainer = this.tabdata1[0].data;
        this.posfromload = false;
      }

      this.firstload = false;
  }
  getIcon(index){
    return this.curentpage > 0 ? '/assets/images/icones/icn ('+(index+1)+').svg':'/assets/images/icones/icn ('+(index+4)+').svg';
  }
  setPage(page){
    this.curentpage = page;
    this.changePage();
  }
  setCurentTab(tIndex){
      let size = this.curentpage > 0 ? this.tabdata1.length : this.tabdata2.length;
      for (let i = 0; i < size; i++){
        if(tIndex == i){
          this.menucontainer = this.showingtab[i].data;
          this.showingtab[i].c2 = this.color2;
          this.showingtab[i].bclass = "container mcustumbt mycusbtn d-flex d-sm-flex d-md-flex flex-column flex-sm-column flex-md-row hvr-bubble-float-right";
          if(this.curentpage > 0) this.tabdata1 = this.showingtab; else this.tabdata2 = this.showingtab;
        }
        else{
          this.showingtab[i].c2 = this.color2+" stinactive";
          this.showingtab[i].bclass = "container mcustumbt mycusbtn d-flex d-sm-flex d-md-flex flex-column flex-sm-column flex-md-row inactive hvr-bubble-float-right2";
        }
      };
  }
  changePage(){
    if(this.curentpage > 0){
      this.bannerlink = "headimg d-flex flex-row justify-content-between";
      this.backlink = "maincont d-flex flex-column";
      this.color2 = "mybtstyle btstylelg btstylemd";
      this.tabTextColor = "bttext";
      this.initdatas();
      this.showingtab = this.tabdata1;
      this.menucontainer = this.tabdata1[0].data;
    }
    else{
      this.bannerlink = "headimg2 d-flex flex-row justify-content-between";
      this.backlink = "maincont2 d-flex flex-column";
      this.color2 = "mybtstyle2 btstylelg btstylemd";
      this.tabTextColor = "bttext2";
      this.initdatas();
      this.showingtab = this.tabdata2;
      this.menucontainer = this.tabdata2[0].data;
    }
    //console.log("curentPage ===>",this.curentpage);
  }

}